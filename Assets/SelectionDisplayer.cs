﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class SelectionDisplayer : MonoBehaviour
{
    public TextMeshProUGUI text;
    private void Start()
    {
        text.text = "Green Ships: " + Selection.data.greenShips + "\n Red Ships: " + Selection.data.redShips;
    }
}
