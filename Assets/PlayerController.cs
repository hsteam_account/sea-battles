﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using System;
namespace SeaBattles
{
    public class PlayerController : MonoBehaviour
    {
        [Serializable]
        public class ShipSelection {
            public PlayerController playerController;
            public List<Ship> items;

            public ShipSelection(PlayerController playerController)
            {
                this.playerController = playerController;
                items = new List<Ship>();
            }
            public void Add(Ship ship)
            {
                if (!items.Contains(ship))
                {
                    items.Add(ship);
                    ship.Select(playerController);
                }

            }
            public void Remove(Ship ship)
            {
                if (items.Contains(ship))
                {
                    items.Remove(ship);
                    ship.Deselect(playerController);
                }

            }
            public void Select(params Ship[] items)
            {
                var newItems = new List<Ship>();
                for (int i = 0; i < items.Length; i++)
                {
                    newItems.Add(items[i]);
                    if (this.items.Contains(items[i]))
                    {
                        this.items.Remove(items[i]);
                    }
                    else
                    {
                        items[i].Select(playerController);
                    }
                }
                foreach (var item in this.items)
                {
                    item.Deselect(playerController);
                }
                this.items = newItems;
            }
            public void DeselectAll()
            {
                foreach (var item in items)
                {
                    item.Deselect(playerController);
                }
                items = new List<Ship>();
            }

            public void Toggle(Ship ship)
            {
                if (items.Contains(ship))
                {
                    ship.Deselect(playerController);
                    items.Remove(ship);
                }
                else
                {
                    ship.Select(playerController);
                    items.Add(ship);
                }
            }
        }
        PhotonView photonView;
        public bool online;
        public ShipSelection shipSelection;
        public RectTransform panel;
        public Canvas canvas;
        public List<Ship> selectableShips = new List<Ship>();
        bool clicked;
        public static PlayerController instance;
        public Ship[] ships;
        Vector2 startOfBox;
        Ship directSelect=null;
        public  PlayerController()
        {
            shipSelection = new ShipSelection(this);
            instance = this;
        }
        // Start is called before the first frame update
        void Start()
        {
            SpawnShips();
        }

        // Update is called once per frame
        void Update()
        {
            if ((!online || photonView.IsMine))
            {
                
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    clicked = true;
                    startOfBox = Input.mousePosition;
                    directSelect=CheckForSingleSelection();


                }
                if (Input.GetKey(KeyCode.Mouse0))
                {
                    Bounds b = CalculateSelectionBounds();

                    b = DrawRectangle(b);

                    CheckForBoxSelections(b);

                }
                else
                {
                    clicked = false;
                    panel.gameObject.SetActive(false);
                }
                if (Input.GetKey(KeyCode.Mouse1))
                {
                    Vector3 mouse = Input.mousePosition;
                    Ray castPoint = Camera.main.ScreenPointToRay(mouse);
                    RaycastHit hit;
                    if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
                    {
                        foreach (var item in shipSelection.items)
                        {
                            item.Move(hit.point, Quaternion.identity);
                        }
                    }
                }
            }
        }

        private Bounds CalculateSelectionBounds()
        {
            Bounds b = new Bounds();
            b.center = Vector3.Lerp(startOfBox, Input.mousePosition, 0.5f);
            b.size = new Vector3(Mathf.Abs(startOfBox.x - Input.mousePosition.x), Mathf.Abs(startOfBox.y - Input.mousePosition.y), 0);
            return b;
        }

        private Bounds DrawRectangle(Bounds b)
        {
            panel.position = b.center;
            panel.sizeDelta = canvas.transform.InverseTransformVector(b.size);
            panel.gameObject.SetActive(true);
            return b;
        }

        private void CheckForBoxSelections(Bounds b)
        {
            foreach (var selectable in selectableShips)
            {
                if (selectable == directSelect)
                {
                    continue;
                }
                Vector3 screenPos = Camera.main.WorldToScreenPoint(selectable.transform.position);
                screenPos.z = 0;

                if (b.Contains(screenPos))
                {
                    shipSelection.Add(selectable);
                }
                else
                {
                    if (!Input.GetKey(KeyCode.LeftControl))
                    {
                        shipSelection.Remove(selectable);
                    }
                }
            }
        }

        private Ship CheckForSingleSelection()
        {
            Vector3 mouse = Input.mousePosition;
            Ray castPoint = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;
            if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
            {
                Ship ship = hit.collider.GetComponent<Ship>();
                if (ship)
                {
                    shipSelection.Toggle(ship);
                    return ship;
                }
            }
            return null;
        }
        [PunRPC]
        void SpawnShips()
        {
            for (int i = 0; i < ships.Length; i++)
            {
                Instantiate(ships[i], new Vector3(10 * i, 0, 0), Quaternion.identity);
            }
        }
    }
}

