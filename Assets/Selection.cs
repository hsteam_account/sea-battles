﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selection 
{
    public static Data data;
    public class Data
    {
        public int greenShips;
        public int redShips;
    }
}
