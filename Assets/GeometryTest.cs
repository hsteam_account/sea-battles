﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeometryTest : MonoBehaviour
{
    public Transform destination;
    public float radius;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnDrawGizmos()
    {
        DrawTriangle(transform.position, destination.position, out  Vector3 r );
    }

    private Vector3 DrawTriangle(Vector3 position, Vector3 destination, out Vector3 r)
    {
        Vector3 h = destination - position;
        float phi = Mathf.Asin(2 * radius / h.magnitude);
        float gLength = Mathf.Cos(phi) * h.magnitude;
        Vector3 g = Quaternion.AngleAxis(-Mathf.Rad2Deg * phi, Vector3.up) * h;
        Gizmos.DrawLine(position, position + h);
        Gizmos.color = Color.red;
        g = g.normalized * gLength;
        Gizmos.DrawLine(position, position + g);
        r = Vector3.Cross(Vector3.up, g).normalized * radius;
        Gizmos.DrawLine(position + g, position + g + (2 * r));
        return position + g;
    }
}
