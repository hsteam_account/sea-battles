﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace SeaBattles
{
    public class SelectionMenu : MonoBehaviour
    {
        private void Start()
        {
            Selection.data = new Selection.Data();
        }
        public void SelectRed()
        {
            Selection.data.redShips++;
        }
        public void SelectGreen()
        {
            Selection.data.greenShips++;
        }
        public void OnPressedReady()
        {
            Hashtable props = new Hashtable
            {
                { SeaBattles.PLAYER_SELECTION_READY, true }
            };
            PhotonNetwork.LocalPlayer.SetCustomProperties(props);
        }
    }
}