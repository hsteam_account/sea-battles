﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Path
{
    public bool startLeftTurn, destinationLeftTurn;
    public Vector3 c1,c2, r,gStart, gEnd, g;
    public Transform destination, start;
    public int sign;
    public PathState state;
    public float radius;
    public float startAngle, destinationAngle;
    public float firstTurnTime, straightLineTime, secondTurnTime;
    public PathMode mode;
}
public enum PathState
{
    FirstTurn,
    straightLine,
    SecondTurn
}
public enum PathMode
{
    ZigZag,
    DoubleTurn
}
public class PathCalculator 
{
    public static Path CalculatePath(MoveAndRotate agent, Transform destination)
    {
        var path = new Path();
        var transform = agent.transform;

        path.start = transform;
        path.destination = destination;

        path.radius = agent.speed / agent.rotationSpeed;

        Vector3 fromTo = destination.position - transform.position;
        var localFT = agent.transform.InverseTransformDirection(fromTo);
        path.sign = localFT.x > 0 ? 1 : -1;
        path.startLeftTurn = path.sign == -1;
        Vector3 r1 = transform.right * path.radius;
        Vector3 r2 = destination.right * path.radius;

        path.c1 = transform.position + path.sign * r1;
        path.c2 = destination.position + path.sign * r2;

        DoubleTurn(path, out Vector3 gStartD, out Vector3 gEndD, out Vector3 gD, out Vector3 rD);
        path.c2 = destination.position - path.sign * r2;
        ZigZag(path, out Vector3 gStart, out Vector3 gEnd, out Vector3 g, out Vector3 r);

        if (gD.magnitude < g.magnitude)
        {
            path.g = gD;
            path.gStart = gStartD;
            path.gEnd = gEndD;
            path.r = rD;
            path.c2 = destination.position + path.sign * r2;
            path.mode = PathMode.DoubleTurn;
            path.destinationLeftTurn = path.startLeftTurn;
        }
        else
        {
            path.g = g;
            path.gStart = gStart;
            path.gEnd = gEnd;
            path.r = r;
            path.mode = PathMode.ZigZag;
            path.destinationLeftTurn = !path.startLeftTurn;

        }
        float beginAngle, destinationAngle=0;
        if (path.startLeftTurn)
        {
             beginAngle = Vector3.SignedAngle(r1, path.r, -Vector3.up);
        }
        else
        {
            beginAngle = Vector3.SignedAngle(-r1, path.r, Vector3.up);
        }
        if (beginAngle < 0)
        {
            beginAngle += 360;
        }

        switch (path.mode)
        {
            case PathMode.ZigZag:
                if (path.destinationLeftTurn)
                {
                    destinationAngle = Vector3.SignedAngle(-r2, path.r, Vector3.up);
                }
                else
                {
                    destinationAngle = Vector3.SignedAngle(r2, path.r, -Vector3.up);
                }
                break;
            case PathMode.DoubleTurn:
                if (path.destinationLeftTurn)
                {
                    destinationAngle = Vector3.SignedAngle(r2, path.r, Vector3.up);
                }
                else
                {
                    destinationAngle = Vector3.SignedAngle(-r2, path.r, -Vector3.up);
                }
                break;
            default:
                break;
        }

        if (destinationAngle < 0)
        {
            destinationAngle += 360;
        }

        path.startAngle = beginAngle;
        path.destinationAngle = destinationAngle;
        path.firstTurnTime  =Mathf.Deg2Rad* path.startAngle / agent.rotationSpeed;
        path.straightLineTime = path.g.magnitude / agent.maxSpeed;
        path.secondTurnTime = Mathf.Deg2Rad * path.destinationAngle / agent.rotationSpeed;
        return path;
    }
    private static void ZigZag(Path path, out Vector3 gStart, out Vector3 gEnd, out Vector3 g, out Vector3 r)
    {
        Vector3 third = DrawTriangle(path, out r);
        gEnd = third + r;
        gStart = path.c1 + r;
        g = gEnd - gStart;
    }

    private static void DoubleTurn(Path path, out Vector3 gStart, out Vector3 gEnd, out Vector3 g, out Vector3 r)
    {
        Vector3 gParallel = path.c2 - path.c1;
        Vector3 rHelp = Vector3.Cross(Vector3.up, gParallel);
        r = -path.sign * rHelp.normalized * path.radius;
        gStart = path.c1 + r;
        gEnd = path.c2 + r;
        g = gEnd - gStart;
    }

    private static Vector3 DrawTriangle(Path path, out Vector3 r)
    {
        Vector3 h = path.c2 - path.c1;
        float phi = Mathf.Asin(2 * path.radius / h.magnitude);
        float gLength = Mathf.Cos(phi) * h.magnitude;
        Vector3 g = Quaternion.AngleAxis(path.sign * Mathf.Rad2Deg * phi, Vector3.up) * h;
        g = g.normalized * gLength;
        r = -path.sign * Vector3.Cross(Vector3.up, g).normalized * path.radius;

        return path.c1 + g;
    }
}
