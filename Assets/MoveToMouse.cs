﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveToMouse : MonoBehaviour
{
    PhotonView photonView;
    NavMeshAgent navMeshAgent;
    NavMeshPath path;
    new Rigidbody rigidbody;

    public Transform arrow;
    private int currentCorner;
    private float cornerMinDistance = 2;
    public float middleCornerIgnore = 5;
    private bool online;
    private bool reachedGoal;
    [Range(0, 1)]
    public float speed = .1f;
    public AnimationCurve speedToRotaion;
    public float maxVelocity = 10;
    public float maxTurnSpeed;

    float turnSpeed;
    private float addedSpeed = 0;
    private float lastAngle;
    private bool rotating;
    private Vector3 rotationVector;
    private bool reachedPosition;
    List<Vector3> points;
    private bool createdArray;

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
        if (photonView != null)
        {
            online = true;
        }
        navMeshAgent = GetComponent<NavMeshAgent>();
        rigidbody = GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update
    void Start()
    {
        path = new NavMeshPath();

    }

    // Update is called once per frame
    void Update()
    {
        turnSpeed = maxTurnSpeed * speedToRotaion.keys[1].time;

        if ((!online || photonView.IsMine))
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                Vector3 mouse = Input.mousePosition;
                Ray castPoint = Camera.main.ScreenPointToRay(mouse);
                RaycastHit hit;
                if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
                {
                    navMeshAgent.CalculatePath(hit.point, path);
                    currentCorner = 1;
                    reachedGoal = false;
                    rotating = false;
                }
            }
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                Vector3 mouse = Input.mousePosition;
                Ray castPoint = Camera.main.ScreenPointToRay(mouse);
                RaycastHit hit;
                if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
                {
                    arrow.position = hit.point;
                }
            }
            if (Input.GetKey(KeyCode.Mouse1))
            {
                Vector3 mouse = Input.mousePosition;
                Ray castPoint = Camera.main.ScreenPointToRay(mouse);
                RaycastHit hit;
                if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
                {
                    arrow.LookAt(hit.point);
                    rotationVector = Vector3.ProjectOnPlane(hit.point - arrow.position, Vector3.up).normalized ;
                }
            }
            if (Input.GetKeyUp(KeyCode.Mouse1))
            {
                rotating = true;
                reachedPosition = false;
                navMeshAgent.CalculatePath(arrow.position, path);
                currentCorner = 1;
                reachedGoal = false;
            }
        }

    }
    private void FixedUpdate()
    {
        if (path.status == NavMeshPathStatus.PathComplete)
        {
            points = new List<Vector3>();
            points.AddRange(path.corners);
            if (rotating)
            {
                Vector3 goal = points[points.Count - 1];
                points.RemoveAt(points.Count - 1);
                points.Add(goal - rotationVector * 6);
                points.Add( goal- rotationVector * 3);
                points.Add( goal);
            }
            createdArray = true;
        }
        if (createdArray && !reachedGoal)
        {

            Vector3 nextCorner = points[currentCorner];
            if (Vector3.Distance(transform.position, nextCorner) < cornerMinDistance)
            {
                currentCorner++;
                if (currentCorner == points.Count)
                {
                    reachedGoal = true;
                    rigidbody.velocity = Vector3.zero;
                    rigidbody.angularVelocity = Vector3.zero;
                    return;
                }
            }
            //transform.LookAt(path.corners[currentCorner]);
            Debug.DrawLine(transform.position, nextCorner, Color.red, Time.fixedDeltaTime);
            Debug.DrawRay(transform.position, transform.forward, Color.blue, Time.fixedDeltaTime);

            float angle = Mathf.Deg2Rad * Vector3.SignedAngle(transform.forward, nextCorner - transform.position, Vector3.up);

            float v = speedToRotaion.Evaluate(rigidbody.velocity.magnitude / maxVelocity) * turnSpeed;
            float y = 0;
            if (Mathf.Abs(angle) > Time.deltaTime * 10 && Mathf.Sign(lastAngle) == Mathf.Sign(angle))
            {
                y = v * Mathf.Sign(angle);
            }
            lastAngle = angle;
            rigidbody.angularVelocity = new Vector3(0, y);

            if (CanTurn(transform.position, nextCorner) || ShouldMoveOn(nextCorner))
            {
                addedSpeed += Time.fixedDeltaTime;
            }
            else
            {
                addedSpeed -= Time.fixedDeltaTime;
            }
            addedSpeed = Mathf.Max(addedSpeed, 0);
            rigidbody.velocity = transform.forward * Mathf.Clamp01(speedToRotaion.keys[1].time + addedSpeed) * maxVelocity;
        }


    }

    private bool ShouldMoveOn(Vector3 nextCorner)
    {
        return false;
        //return (currentCorner < path.corners.Length - 1 && Vector3.Distance(transform.position, nextCorner) < middleCornerIgnore);
        //return (currentCorner < path.corners.Length - 1 &&CanTurn(transform.position, path.corners[currentCorner+1], speed*3)&&Vector3.Distance(transform.position, nextCorner)<middleCornerIgnore);
        //return (currentCorner < path.corners.Length - 1 &&CanTurn( transform.position, nextCorner, speed * 100 )&&( Vector3.Distance(transform.position, nextCorner) < middleCornerIgnore));

    }
    private bool CanTurn(Vector3 position, Vector3 destination)
    {
        return CanTurn(position, destination, speed);
    }
    private bool CanTurn(Vector3 position, Vector3 destination, float speed)
    {
        if (rigidbody.angularVelocity.y.AlmostEquals(0, float.Epsilon)|| rigidbody.velocity ==Vector3.zero)
        {
            return true;
        }

        Vector3 resVector = new Vector3();
        Vector3 prevResVector = new Vector3();
        Vector3 projectedVector = new Vector3();
        Vector3 onNormal = destination - position;
        Vector3 velocity = rigidbody.velocity;
        var projectedAngle = Vector3.Angle(Vector3.Project(velocity, onNormal), onNormal);
        int i = 0;
        if (projectedAngle > 170)
        {
            return false;
        }
        while (projectedVector.magnitude / onNormal.magnitude < 1)
        {
            prevResVector = resVector;
            resVector += Quaternion.AngleAxis(Mathf.Rad2Deg * rigidbody.angularVelocity.y * i * speed, Vector3.up) * velocity;
            Debug.DrawLine(position + prevResVector, position + resVector, Color.green, Time.fixedDeltaTime);
            projectedVector = Vector3.Project(resVector, onNormal);
            if (Vector3.Project(resVector, onNormal).magnitude < Vector3.Project(velocity, onNormal).magnitude)
            {
                return true;
            }
            i++;
            if (i>1000)
            {
                throw new Exception("over 1000");
            }
        }
        Plane plane = new Plane(Vector3.zero, Vector3.up, onNormal);
        var v1 = position;
        var v2 = position + Vector3.up;
        var v3 = position + onNormal;


        Debug.DrawLine(v1, v2, Color.blue, Time.fixedDeltaTime);
        Debug.DrawLine(v2, v3, Color.blue, Time.fixedDeltaTime);
        Debug.DrawLine(v3, v1, Color.blue, Time.fixedDeltaTime);
        return !plane.SameSide(velocity, resVector);

    }
}
