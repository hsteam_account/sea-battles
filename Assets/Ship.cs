﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SeaBattles
{
    public interface Selectable
    {
        void Select(PlayerController playerController);
        void Deselect(PlayerController playerController);
    }
    
    public class Ship : MonoBehaviour, Selectable
    {
        MoveAndRotate moveAndRotate;
        public Transform selectionMarker;
        public Destination destination;
        private void Awake()
        {
            moveAndRotate = GetComponent<MoveAndRotate>();
            moveAndRotate.destination = destination.transform;
            destination.ship = this;
        }
        private void OnEnable()
        {
            //TODO add Photon View
            PlayerController.instance.selectableShips.Add(this);
            destination.transform.parent = null;
        }
        public void Move(Vector3 position, Quaternion rotation)
        {
            destination.Set(position, rotation);
            moveAndRotate.MoveToDestination();
        }
        public void Select(PlayerController playerController)
        {
            selectionMarker.gameObject.SetActive(true);
        }
        public void Deselect(PlayerController playerController)
        {
            selectionMarker.gameObject.SetActive(false);
        }
    }
}
