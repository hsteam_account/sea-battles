﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SeaBattles
{
    public class Destination : MonoBehaviour
    {
        public LineRenderer lineRenderer;
        [HideInInspector]
        public Ship ship;
        private void Awake()
        {
        }
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            var positions = new Vector3[] { 
                transform.position,
                ship.transform.position
            };
            lineRenderer.SetPositions(positions);
        }

        public void Set(Vector3 position, Quaternion rotation)
        {
            transform.position = position;
            transform.rotation = rotation;
        }
    }
}