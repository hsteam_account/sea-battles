﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace SeaBattles
{
    public class Spawner : MonoBehaviourPunCallbacks
    {
        public Transform[] spawnPoints;
        public Transform objectToSpawn;
        private void Awake()
        {
            SeaBattlesManager.Instance.spawner = this;

        }
        private void Start()
        {
            Hashtable props = new Hashtable
            {
                { SeaBattles.PLAYER_LOADED_MAP,true }
            };
            PhotonNetwork.LocalPlayer.SetCustomProperties(props);
        }

        public void Spawn(int v)
        {
            //PhotonNetwork.Instantiate(objectToSpawn.name, spawnPoints[v].position, spawnPoints[v].rotation);
        }
    }
}