﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Photon.Pun;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class MoveAndRotate : MonoBehaviour
{
    PhotonView photonView;
    NavMeshAgent navMeshAgent;
    NavMeshPath navMeshPath;
    new Rigidbody rigidbody;
    private bool online;

    [HideInInspector]
    public Transform destination;
    public float speed, rotationSpeed, maxSpeed;
    public Path path;
    public float[] speeds =
    {
        1, 5, 20
    };
    private float startTime;
    public int speedIndex=2;
    public int curveSpeedIndex=2;
    [HideInInspector]
    public int currentSpeedIndex=2;
    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
        if (photonView != null)
        {
            online = true;
        }
        navMeshAgent = GetComponent<NavMeshAgent>();
        rigidbody = GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public void MoveToDestination()
    {
            speed = speeds[curveSpeedIndex];
            maxSpeed = speeds[speedIndex];
            currentSpeedIndex = curveSpeedIndex;
            path = PathCalculator.CalculatePath(this, destination);
            while (float.IsNaN (path.g.magnitude))
            {
                currentSpeedIndex--;
                speed = speeds[currentSpeedIndex];
                path = PathCalculator.CalculatePath(this, destination);

            }

            startTime = Time.fixedTime;
    }
    private void FixedUpdate()
    {
        if (path!=null)
        {
            rigidbody.velocity =Vector3.ProjectOnPlane( transform.forward, Vector3.up) * speed;
            switch (path.state)
            {
                case PathState.FirstTurn:
                    Turn(path.startLeftTurn);
                    if (Time.fixedTime - startTime >  path.firstTurnTime)
                    {
                        startTime = Time.fixedTime;
                        speed =maxSpeed;
                        path.state = PathState.straightLine;
                    }
                    break;
                case PathState.straightLine:
                    rigidbody.angularVelocity = Vector3.zero;
                    if (Time.fixedTime - startTime> path.straightLineTime)
                    {
                        startTime = Time.fixedTime;
                        speed = speeds[currentSpeedIndex];
                        path.state = PathState.SecondTurn;
                    }
                    break;
                case PathState.SecondTurn:
                    Turn(path.destinationLeftTurn);
                    if (Time.fixedTime - startTime > path.secondTurnTime)
                    {
                        startTime = Time.fixedTime;
                        path = null;
                    }
                    break;
                default:
                    break;
            }
        }
        else
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;

        }
    }

    private void Turn(bool left)
    {
        if (left)
        {
            rigidbody.angularVelocity = new Vector3(rigidbody.angularVelocity.x, -rotationSpeed, rigidbody.angularVelocity.z);
        }
        else
        {
            rigidbody.angularVelocity = new Vector3(rigidbody.angularVelocity.x, rotationSpeed, rigidbody.angularVelocity.z);
        }
    }
}
